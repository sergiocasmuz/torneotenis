# Tenis Docker


## Empecemos

1. Si aún no lo has hecho, instala Docker Compose (v2.10+).
2. Ejecuta `docker compose build --no-cache` para construir imágenes nuevas desde cero.
3. Ejecuta `docker compose up -d` para iniciar el proyecto.
4. Abre `https://localhost:3012` en tu navegador y [acepta el certificado TLS generado automáticamente.](https://stackoverflow.com/a/15076602/1352334)
5. Ejecuta `echo yes | docker exec -i tenisgeo-php-1 php bin/console doctrine:fixtures:load` para correr los fixtures, esto cargará jugadores en el sistema también puedes usar este comando para reiniciar el sistema

## Información adicional

La app utilaza un contenedor docker que Symfony 7 y una base de datos postgres.
Se utilizó una arquitectura MVC para resolver la vista rapida de una interfaz symfony.
Se creó 2 comandos y 3 servicios para la logica general, separar esta de los controladores para mantenerlos lo mas limpios posibles y dividir las responsabilidades de la app.

- Para el correcto funcionamiento se debe tener en cuenta:
1. Haber corrido el fixture para que se carguen los jugadores
2. Crear un torneo
3. Realizar la inscripcion correspondiente desde "inscripciones"
4. acceder al torneo y simular el torneo