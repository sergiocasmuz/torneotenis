<?php

namespace App\Form;

use App\Entity\Jugador;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JugadorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('genero', ChoiceType::class, [
                'choices' => [
                    'Masculino' => true,
                    'Femenino' => false,
                ],
            ])
            ->add('saque')
            ->add('fondo')
            ->add('volea')
            ->add('movilidad')
            ->add('resistencia')
            ->add('velocidad')
            ->add('fuerza')
            ->add('reaccion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Jugador::class,
        ]);
    }
}
