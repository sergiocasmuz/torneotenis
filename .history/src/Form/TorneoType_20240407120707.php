<?php

namespace App\Form;

use App\Entity\Torneo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class TorneoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('genero', ChoiceType::class, [
                'choices' => [
                    'Masculino' => true,
                    'Femenino' => false,
                ],
            ])
            ->add('cantidad', ChoiceType::class, [
                'choices' => [
                    '4 Jugadores' => 4,
                    '8 Jugadores' => 8,
                    '16 Jugadores' => 16,
                    '32 Jugadores' => 32,
                    '64 Jugadores' => 64,
                ],
            ])
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $form->getData();

            if ($data->getCantidad() !=) {
                $cantidad = $data->getCantidad();
                $etapas = log($cantidad, 2);
                $etapas = ceil($etapas);

                // Establecemos el valor de las etapas en los datos del formulario
                $data['etapas'] = $etapas;
                $event->setData($data);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Torneo::class,
        ]);
    }
}
