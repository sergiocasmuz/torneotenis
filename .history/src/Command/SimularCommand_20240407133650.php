<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'Simular',
    description: 'Este command se encarga de realizar la simulacion con las ditintas estapas',
)]
class SimularCommand extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('torneo', InputArgument::OPTIONAL, 'Torneo')
            ->addArgument('etapa', InputArgument::OPTIONAL, 'Etapa')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $torneo = $input->getArgument('torne');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }


        $torneo = $em->getRepository(Torneo::class)->find(1);
        $etapas = $torneo->getEtapas();

        for($etapa=1; $etapas >= $etapa; $etapa++){
            $organiza->organiza(1,$etapa);
            $simular->Etapa(1,$etapa);
        }


        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
