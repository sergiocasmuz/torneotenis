<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Torneo;
use App\Service\Inscripciones;


#[AsCommand(
    name: 'Inscripciones',
    description: 'Este command se encarga de realizar inscripciones aleatoreas según el permita el torneo',
)]
class InscripcionesCommand extends Command
{
    private $em;
    private $InscripcionesService;

    public function __construct(EntityManagerInterface $em, Inscripciones $InscripcionesService)
    {
        parent::__construct();
        $this->em = $em;
        $this->InscripcionesService = $InscripcionesService;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('torneo', InputArgument::OPTIONAL, 'Torneo')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $idtorneo = $input->getArgument('torneo');

        if ($idtorneo) {
            $io->note(sprintf('You passed an argument: %s', $idtorneo));
        }

        $torneo = $this->em->getRepository(Torneo::class)->find($idtorneo);

        $this->InscripcionesService->inscripcionesAleatoreas($idtorneo);
        

        $output->writeln(sprintf('El ganador en la etapa más alta es: %s', $campeon ));

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
