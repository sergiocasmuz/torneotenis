<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\SimularTorneo;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Torneo;
use App\Service\OrganizarPartidos;

#[AsCommand(
    name: 'Simular',
    description: 'Este command se encarga de realizar la simulacion con las ditintas estapas',
)]
class SimularCommand extends Command
{
    private $em;
    private $simularTorneo;
    private $organiza;

    public function __construct(EntityManagerInterface $em, SimularTorneo $simularTorneo)
    {
        parent::__construct();
        $this->em = $em;
        $this->simularTorneo = $simularTorneo;
        $this->
    }

    protected function configure(): void
    {
        $this
            ->addArgument('torneo', InputArgument::OPTIONAL, 'Torneo')
            ->addArgument('etapa', InputArgument::OPTIONAL, 'Etapa')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $idtorneo = $input->getArgument('torneo');
        $idetapa = $input->getArgument('etapa');

        if ($idtorneo) {
            $io->note(sprintf('You passed an argument: %s', $idtorneo));
        }

        if ($idetapa) {
            $io->note(sprintf('You passed an argument: %s', $idetapa));
        }

        if ($input->getOption('option1')) {
            // ...
        }


        $torneo = $this->em->getRepository(Torneo::class)->find($idtorneo);
        $etapas = $torneo->getEtapas();

        for($etapa=1; $etapas >= $etapa; $etapa++){
            $organiza->organiza(1,$etapa);
            $simular->Etapa(1,$etapa);
        }


        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
