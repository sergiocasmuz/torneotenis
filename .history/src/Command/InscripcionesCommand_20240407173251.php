<?php

namespace App\Command;

use App\Entity\Resultado;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\SimularTorneo;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Torneo;
use App\Service\OrganizarPartidos;


#[AsCommand(
    name: 'Inscripciones',
    description: 'Este command se encarga de realizar inscripciones aleatoreas según el per',
)]
class SimularCommand extends Command
{
    private $em;
    private $simularTorneo;
    private $organiza;

    public function __construct(EntityManagerInterface $em, SimularTorneo $simularTorneo, OrganizarPartidos $organiza)
    {
        parent::__construct();
        $this->em = $em;
        $this->simularTorneo = $simularTorneo;
        $this->organiza = $organiza;
;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('torneo', InputArgument::OPTIONAL, 'Torneo')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $idtorneo = $input->getArgument('torneo');

        if ($idtorneo) {
            $io->note(sprintf('You passed an argument: %s', $idtorneo));
        }


     


        $torneo = $this->em->getRepository(Torneo::class)->find($idtorneo);
        $etapas = $torneo->getEtapas();

        for($etapa=1; $etapas >= $etapa; $etapa++){
            $this->organiza->organiza($torneo->getId(),$etapa);
            $this->simularTorneo->Etapa($torneo->getId(),$etapa);
        }

        
        $campeon = $this->em->getRepository(Resultado::class)->findOneBy(['etapa'=>4]);
        $campeon = $campeon->getGanador()->getNombre()." ".$campeon->getGanador()->getApellido();
        

        $output->writeln(sprintf('El ganador en la etapa más alta es: %s', $campeon ));

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
