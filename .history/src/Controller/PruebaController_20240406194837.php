<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em): Response
    {

        $torneo = $em->getRepository(Torneo::class)->find(1);
      
        //vamos a generar los paridos iniciales
        $inscriptos = $em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>$torneo]);

        
        
        $offset = 0;

        do {
            $partidosCreados = false; // Reiniciar la variable en cada iteración del bucle interior
        
            $contador = $offset;
            while ($contador < min($offset + 100, count($inscriptos))) {
                $control = $em->getRepository(Partido::class)->findBy([
                    'torneo' => $torneo, 
                    'jugador_1' => $inscriptos[$contador]->getJugador1(),
                    'jugador_2' => $inscriptos[$contador+1]->getJugador1()
                ]);
        
                if (count($control) == 0) {
                    $partido = new Partido();
                    $partido->setJugador1($inscriptos[$contador]->getJugador1());
                    $partido->setJugador2($inscriptos[$contador+1]->getJugador1());
                    $partido->setTorneo($torneo);
                    $em->persist($partido);
                    $partidosCreados = true; 
                }
        
                $contador += 2; 
            }
        
            // Realizar el flush solo si se han creado partidos nuevos
            if ($partidosCreados) {
                $em->flush();
            }
        
            $em->clear(); 
        
            $offset = $contador;
        
        } while ($offset < count($inscriptos));

       

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
