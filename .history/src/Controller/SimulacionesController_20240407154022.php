<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ResultadoRepository;
use Doctrine\ORM\EntityManagerInterface;

class SimulacionesController extends AbstractController
{

    #[Route('/simulaciones', name: 'app_simulaciones')]
    public function simulateAction(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $idTorneo = $data['torneo'] ?? null;
        if ($idTorneo === null ) {
            return new JsonResponse(['error' => 'Se requieren el ID del torneo y la etapa.'], JsonResponse::HTTP_BAD_REQUEST);
        }

        $command = sprintf('php %s/bin/console Simular %d', $this->getParameter('kernel.project_dir'), $idTorneo);

        // Crear una instancia de Process
        $process = new Process(explode(' ', $command));

        // Ejecutar el comando
        $process->run();

        if (!$process->isSuccessful()) {
            // Devolver un error si el comando falló
            return new JsonResponse(['error' => 'Error al ejecutar el comando.'], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        $campeon = $this->em->
      

        $output = $process->getOutput();

        return new JsonResponse(['message' => 'La sumulación fue esxitosa.', 'output' => $output]);
    }
}
