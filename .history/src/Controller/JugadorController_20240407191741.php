<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Jugador;

class JugadorController extends AbstractController
{
    #[Route('/jugador', name: 'app_jugador')]
    public function index(EntityManagerInterface $em): Response
    {
    
        $jugadores = $em->getRepository(Jugador::class)->findAll();

        return $this->render('jugador/index.html.twig', [
            'controller_name' => 'JugadorController',
            'kugadores'
        ]);
    }
}
