<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Request;

class SimulacionesController extends AbstractController
{
    #[Route('/simulaciones', name: 'app_simulaciones')]
    public function simulateAction(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $idTorneo = $data['torneo'] ?? null;
        if ($idTorneo === null ) {
            return new JsonResponse(['error' => 'Se requieren el ID del torneo y la etapa.'], JsonResponse::HTTP_BAD_REQUEST);
        }

        // Construir el comando
        $command = sprintf('php %s/bin/console Simular %d %d --option1', $this->getParameter('kernel.project_dir'), $idTorneo, $etapa);

        // Crear una instancia de Process
        $process = new Process(explode(' ', $command));

        // Ejecutar el comando
        $process->run();

        // Verificar si hubo algún error al ejecutar el comando
        if (!$process->isSuccessful()) {
            // Devolver un error si el comando falló
            return new JsonResponse(['error' => 'Error al ejecutar el comando.'], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        // Obtener la salida del comando si es necesario
        $output = $process->getOutput();

        // Devolver una respuesta con los resultados
        return new JsonResponse(['message' => 'Comando ejecutado correctamente.', 'output' => $output]);
    }
}
