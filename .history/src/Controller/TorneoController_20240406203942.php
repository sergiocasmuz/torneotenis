<?php

namespace App\Controller;

use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Service\OrganizarPartidos;

class TorneoController extends AbstractController
{
    #[Route('/torneo', name: 'app_torneo')]
    public function index(EntityManagerInterface $em): Response
    {
    
        
        try {
            $torneos = $em->getRepository(Torneo::class)->findBy([], [], 10); 
        } catch (\Exception $e) {
            // Manejar la excepción de alguna manera, por ejemplo, registrándola o mostrando un mensaje de error al usuario.
            return new Response('Se produjo un error al recuperar los torneos: ' . $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        return $this->render('torneo/index.html.twig', [
           'torneos'=>$torneos
        ]);
    }

    #[Route('/organizar/{etapa}', name: 'app_organizar', methods: ['GET'])]
    public function organizar(EntityManagerInterface $em, $etapa): Response
    {
        dd($etapa);
    }
}
