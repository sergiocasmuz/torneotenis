<?php

namespace App\Controller;

use App\Entity\Torneo;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Form\TorneoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Service\OrganizarPartidos;
use Symfony\Component\HttpFoundation\Request;

class TorneoController extends AbstractController
{
    #[Route('/torneo', name: 'app_torneo')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {    
        try {
            $torneos = $em->getRepository(Torneo::class)->findBy([], [], 10); 
        } catch (\Exception $e) {
            // Manejar la excepción de alguna manera, por ejemplo, registrándola o mostrando un mensaje de error al usuario.
            return new Response('Se produjo un error al recuperar los torneos: ' . $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $torneo = new Torneo();
        $torneoForm = $this->createForm(TorneoType::class, $torneo);
        $torneoForm->handleRequest($request);

        if ($torneoForm->isSubmitted() && $torneoForm->isValid()) {
            $torneo = $torneoForm->getData();

            $em->persist($torneo);
            $em->flush();

            return $this->redirectToRoute('app_torneo');
        }


        return $this->render('torneo/index.html.twig', [
           'torneos'=>$torneos,
           'torneoForm'=>$torneoForm->createView()
        ]);
    }

    #[Route('/torneoDetalla/{torneo}', name: 'app_torneo_detalles', methods: ['GET'])]
    public function organizar(EntityManagerInterface $em,int $torneo, OrganizarPartidos $orgnizar): Response
    {

        $inscriptos = $em->getRepository(InscripcionTorneo)->findBy(['torneo']);
        

        $partidos = $em->getRepository(Partido::class)->findBy(['etapa'=>$etapa, 'torneo'=>$torneo]);
        return $this->render('torneo/detalles.html.twig', [
            'inscriptos' => $inscriptos
         ]);
    }
}
