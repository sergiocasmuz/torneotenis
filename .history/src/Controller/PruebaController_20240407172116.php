<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Jugador;
use App\Entity\Partido;
use App\Entity\Resultado;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\OrganizarPartidos;
use App\Service\SimularTorneo;


class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em, SimularTorneo $simular ,OrganizarPartidos $organiza): Response
    {
        $torneo = $em->getRepository(Torneo::class)->find(2);

        $jugadores = $em->getRepository(Jugador::class)->findAll();

        shuffle($jugadores);
        $maxJugadoresAleatorios = 16;

        $indicesAleatorios = array_rand($jugadores, $maxJugadoresAleatorios);
        $jugadoresInscritos = [];
        foreach ($indicesAleatorios as $indice) {
            $jugadoresInscritos[] = $jugadores[$indice];
        }

        foreach ($jugadoresInscritos as $jugador) {
            $inscripcion = new Inscripcion();
            $inscripcion->setTorneo($torneo);
            $inscripcion->setJugador($jugador);
    
            $em->persist($inscripcion);
        }
    
        $em->flush();
       

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
