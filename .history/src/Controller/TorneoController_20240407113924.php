<?php

namespace App\Controller;

use App\Entity\Torneo;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Form\TorneoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Service\OrganizarPartidos;
use Symfony\Component\HttpFoundation\Request;

class TorneoController extends AbstractController
{
    #[Route('/torneo', name: 'app_torneo')]
    public function index(EntityManagerInterface $em, Request $request): Response
    {    
        try {
            $torneos = $em->getRepository(Torneo::class)->findBy([], [], 10); 
        } catch (\Exception $e) {
            // Manejar la excepción de alguna manera, por ejemplo, registrándola o mostrando un mensaje de error al usuario.
            return new Response('Se produjo un error al recuperar los torneos: ' . $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $torneo = new Torneo();
        $torneoForm = $this->createForm(TorneoType::class, $torneo);

        if ($torneoForm->getisSubmitted() && $torneoForm->isValid()) {
            
            // $nombre   = $torneoForm->get('nombre')->getData();
            // $genero   = $torneoForm->get('genero')->getData();
            // $cantidad = $torneoForm->get('cantidad')->getData();
            // $etapas   = $torneoForm->get('etapas')->getData();

            $torneo = $torneoForm->getData();

    
            $em->persist($torneoForm);
            $em->flush();

            // Redirigimos a donde quieras después de que el formulario se haya enviado exitosamente
            return $this->redirectToRoute('ruta_donde_quieras_redirigir');
        }


        return $this->render('torneo/index.html.twig', [
           'torneos'=>$torneos,
           'torneoForm'=>$torneoForm->createView()
        ]);
    }

    #[Route('/organizar/{torneo}/{etapa}', name: 'app_organizar', methods: ['GET'])]
    public function organizar(EntityManagerInterface $em,int $torneo,int $etapa, OrganizarPartidos $orgnizar): Response
    {
        
        
        $orgnizar->organiza($torneo,$etapa);

        

        $partidos = $em->getRepository(Partido::class)->findBy(['etapa'=>$etapa, 'torneo'=>$torneo]);
        return $this->render('torneo/cargando.html.twig', [
            'partidos' => $partidos
         ]);
    }
}
