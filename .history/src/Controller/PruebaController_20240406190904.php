<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em): Response
    {

        $torneo = $em->getRepository(Torneo::class)->find(1);
      
        //vamos a generar los paridos iniciales
        $inscriptos = $em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>1]);

        
        //alteramos el orden
        shuffle($inscriptos);

        foreach ($inscriptos as $inscripto) {

            $partido = new Partido();
            $partido->setJugador1($inscripto->getJugador1());
            $partido->setJugador2($inscripto->getJugador2());
            $partido->setTorneo($torneo);
            
        }


        dd($inscriptos);

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
