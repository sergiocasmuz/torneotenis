<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Entity\Resultado;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\OrganizarPartidos;
use App\Service\SimularTorneo;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em, SimularTorneo $simular): Response
    {
        $torneo = $em->getRepository(Torneo::class)->find(1);
        $etapas = $torneo->getEtapas();

        for($etapa=1; $etapas > $etapa; $etapa){
            $resultados = $em->getRepository(Resultado::class)->findBy(['torneo'=>$torneo,'etapa'=>$etapa]);
            $inscriptos = [];
            foreach($resultados as $resultado){
                $inscriptos[] = $resultado->getGanador();
            }

        }
       
            dd($inscriptos);

    
           $simular->Etapa(1,1);
       

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
