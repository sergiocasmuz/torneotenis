<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;

class InscripcionController extends AbstractController
{
    #[Route('/inscripcion', name: 'app_inscripcion')]
    public function index(EntityManagerInterface $em): Response
    {

        $inscripciones = $em->getRepository(InscripcionTorneo::class)->findAll();
        $torneos = $em->getRepository(Torneo::class)->createQueryBuilder('t')
    ->orderBy('t.id', 'DESC')
    ->getQuery()
    ->getResult();

    

        return $this->render('inscripcion/index.html.twig', [
            'controller_name' => 'InscripcionController',
            'inscripciones' => $inscripciones,
            'torneos'=>$torneos
        ]);
    }
}
