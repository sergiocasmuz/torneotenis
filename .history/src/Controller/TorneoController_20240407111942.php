<?php

namespace App\Controller;

use App\Entity\Torneo;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Form\TorneoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Service\OrganizarPartidos;

class TorneoController extends AbstractController
{
    #[Route('/torneo', name: 'app_torneo')]
    public function index(EntityManagerInterface $em): Response
    {    
        try {
            $torneos = $em->getRepository(Torneo::class)->findBy([], [], 10); 
        } catch (\Exception $e) {
            // Manejar la excepción de alguna manera, por ejemplo, registrándola o mostrando un mensaje de error al usuario.
            return new Response('Se produjo un error al recuperar los torneos: ' . $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $torneoForm = new TorneoType();

        $torneoForm->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database

            return $this->redirectToRoute('task_success');
        }


        return $this->render('torneo/index.html.twig', [
           'torneos'=>$torneos
        ]);
    }

    #[Route('/organizar/{torneo}/{etapa}', name: 'app_organizar', methods: ['GET'])]
    public function organizar(EntityManagerInterface $em,int $torneo,int $etapa, OrganizarPartidos $orgnizar): Response
    {
        
        
        $orgnizar->organiza($torneo,$etapa);

        

        $partidos = $em->getRepository(Partido::class)->findBy(['etapa'=>$etapa, 'torneo'=>$torneo]);
        return $this->render('torneo/cargando.html.twig', [
            'partidos' => $partidos
         ]);
    }
}
