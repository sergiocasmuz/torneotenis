<?php

namespace App\Controller;

use App\Entity\Partido;
use App\Form\PartidoType;
use App\Repository\PartidoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\VarDumper\VarDumper;

#[Route('/partido')]
class PartidoController extends AbstractController
{
    #[Route('/', name: 'app_partido_index', methods: ['GET'])]
    public function index(PartidoRepository $partidoRepository): Response
    {
        
        $partidos =  $partidoRepository->findAll();
dd()
        return $this->render('partido/index.html.twig', [
            'partidos' => $partidos,
        ]);
    }

   

    #[Route('/{id}', name: 'app_partido_show', methods: ['GET'])]
    public function show(Partido $partido): Response
    {
        return $this->render('partido/show.html.twig', [
            'partido' => $partido,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_partido_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Partido $partido, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(PartidoType::class, $partido);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_partido_index', [], Response::HTTP_SEE_OTHER);
        }


        

        return $this->render('partido/edit.html.twig', [
            'partido' => $partido,
            'form' => $form,
        ]);
    }


}
