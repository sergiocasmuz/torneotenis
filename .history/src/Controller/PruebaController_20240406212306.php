<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\OrganizarPartidos;
use App\Service\SimularPartido;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em, SimularPartido $partidos): Response
    {

        $partidosA = $em->getRepository(Partido::class)->findBy(['torneo'=>1,'etapa'=>1]);


        

        foreach($partidos as $unPartido){
            $partidos->Simular($unPartido->getJugadir1(),$unPartido->getJugador2());
        }
        dd($partidos);
        //$partidos->Simular();

        
       

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
