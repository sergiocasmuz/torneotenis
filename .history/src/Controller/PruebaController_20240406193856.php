<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em): Response
    {

        $torneo = $em->getRepository(Torneo::class)->find(1);
      
        //vamos a generar los paridos iniciales
        $inscriptos = $em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>$torneo]);

        
        
        

        $contador=0;
        while ($contador < count($inscriptos)) {

            $control = $em->getRepository(Partido::class)->findBy([
                'torneo'=>$torneo, 
                'jugador_1'=>$inscriptos[$contador]->getJugador1(),
                'jugador_1'=>$inscriptos[$contador+2]->getJugador1()
            ]);

            if(!$control){

                $partido = new Partido();
                $partido->setJugador1($inscriptos[$contador]->getJugador1());
                $partido->setJugador2($inscriptos[$contador+1]->getJugador1());
                $partido->setTorneo($torneo);
                $contador+=2;

                $em->persist($partido);

            }
        }

        if($partido){}

        $em->flush();

       

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
