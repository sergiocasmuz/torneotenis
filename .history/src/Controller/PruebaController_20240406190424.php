<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em): Response
    {

        //vamos a generar los paridos iniciales
        $inscriptos = $em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>1]);
        
        //
        shuffle($inscriptos);




        dd($inscriptos);

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
