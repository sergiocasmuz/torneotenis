<?php

namespace App\Controller;

use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use App\Entity\Torneo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;

class PruebaController extends AbstractController
{
    #[Route('/prueba', name: 'app_prueba')]
    public function index(EntityManagerInterface $em): Response
    {

        $torneo = $em->getRepository(Torneo::class)->find(1);
      
        //vamos a generar los paridos iniciales
        $inscriptos = $em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>$torneo]);

        
        
        

        do {
            $contador = $offset;
            while ($contador < min($offset + 100, count($inscriptos))) {
                // Procesar registros desde $contador hasta $contador + 99
                // Aquí iría el código para procesar los registros
                $contador += 2; // Avanzar al siguiente par de registros
            }
        
            // Actualizar el índice del último registro procesado
            $offset = $contador;
        
            // Realizar el flush solo si se han creado partidos nuevos
            if ($partidosCreados) {
                $em->flush(); // Solo hacer flush si se han creado partidos nuevos
            }
        
            // Limpiar la memoria después de cada iteración del bucle
            $em->clear(); 
        
        } while ($offset < count($inscriptos));

       

        return $this->render('prueba/index.html.twig', [
            'controller_name' => 'PruebaController',
        ]);
    }
}
