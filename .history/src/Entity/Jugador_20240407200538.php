<?php

namespace App\Entity;

use App\Repository\JugadorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JugadorRepository::class)]
class Jugador
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nombre = null;

    #[ORM\Column(length: 255)]
    private ?string $apellido = null;

    #[ORM\Column]
    private ?bool $genero = null;

    #[ORM\Column(nullable: true)]
    private ?int $saque = null;

    #[ORM\Column]
    private ?int $fondo = null;

    #[ORM\Column]
    private ?int $volea = null;

    #[ORM\Column]
    private ?int $movilidad = null;

    #[ORM\Column]
    private ?int $resistencia = null;

    #[ORM\Column(nullable: true)]
    private ?int $velocidad = null;

    #[ORM\Column(nullable: true)]
    private ?int $fuerza = null;

    #[ORM\Column(nullable: true)]
    private ?int $reaccion = null;

    /**
     * @var Collection<int, InscripcionTorneo>
     */
    #[ORM\OneToMany(targetEntity: InscripcionTorneo::class, mappedBy: 'jugador1')]
    private Collection $inscripcionTorneos;

    public function __construct()
    {
        $this->inscripcionTorneos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    public function setApellido(string $apellido): static
    {
        $this->apellido = $apellido;

        return $this;
    }

    public function isGenero(): ?bool
    {
        return $this->genero;
    }

    public function setGenero(bool $genero): static
    {
        $this->genero = $genero;

        return $this;
    }

    public function getSaque(): ?int
    {
        return $this->saque;
    }

    public function setSaque(?int $saque): static
    {
        $this->saque = $saque;

        return $this;
    }

    public function getFondo(): ?int
    {
        return $this->fondo;
    }

    public function setFondo(int $fondo): static
    {
        $this->fondo = $fondo;

        return $this;
    }

    public function getVolea(): ?int
    {
        return $this->volea;
    }

    public function setVolea(int $volea): static
    {
        $this->volea = $volea;

        return $this;
    }

    public function getMovilidad(): ?int
    {
        return $this->movilidad;
    }

    public function setMovilidad(int $movilidad): static
    {
        $this->movilidad = $movilidad;

        return $this;
    }

    public function getResistencia(): ?int
    {
        return $this->resistencia;
    }

    public function setResistencia(int $resistencia): static
    {
        $this->resistencia = $resistencia;

        return $this;
    }

    public function getVelocidad(): ?int
    {
        return $this->velocidad;
    }

    public function setVelocidad(?int $velocidad): static
    {
        $this->velocidad = $velocidad;

        return $this;
    }

    public function getFuerza(): ?int
    {
        return $this->fuerza;
    }

    public function setFuerza(?int $fuerza): static
    {
        $this->fuerza = $fuerza;

        return $this;
    }

    public function getReaccion(): ?int
    {
        return $this->reaccion;
    }

    public function setReaccion(?int $reaccion): static
    {
        $this->reaccion = $reaccion;

        return $this;
    }

    /**
     * @return Collection<int, InscripcionTorneo>
     */
    public function getInscripcionTorneos(): Collection
    {
        return $this->inscripcionTorneos;
    }

    public function addInscripcionTorneo(InscripcionTorneo $inscripcionTorneo): static
    {
        if (!$this->inscripcionTorneos->contains($inscripcionTorneo)) {
            $this->inscripcionTorneos->add($inscripcionTorneo);
            $inscripcionTorneo->setJugador1($this);
        }

        return $this;
    }

    public function removeInscripcionTorneo(InscripcionTorneo $inscripcionTorneo): static
    {
        if ($this->inscripcionTorneos->removeElement($inscripcionTorneo)) {
            // set the owning side to null (unless already changed)
            if ($inscripcionTorneo->getJugador1() === $this) {
                $inscripcionTorneo->setJugador1(null);
            }
        }

        return $this;
    }


    public function getHabilidad(){
        $habilidad = 0;
        $habilidad += $this->velocidad;
        $habilidad += $this->volea;
        $habilidad += $this->fondo;
        $habilidad += $this->movilidad;
        $habilidad += $this->resistencia;
        $habilidad += $this->fuerza;
        $habilidad += $this->saque;
        $habilidad += $this->reaccion;

        return $habilidad;

    }
}
