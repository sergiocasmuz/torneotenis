<?php

namespace App\Service;

use App\Entity\Jugador;
use App\Entity\Torneo;
use App\Entity\Resultado;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;

class OrganizarPartidos
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function organiza(int $torneo ,int $etapa)
    {
        $torneo = $this->em->getRepository(Torneo::class)->find($torneo);
        $etapas = $torneo->getEtapas();

        if($etapa == 1){
            $inscriptos = $this->em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>$torneo]);
        }else{
            for($etapaC=1; $etapas <= $etapaC; $etapaC++){
                $resultados = $this->em->getRepository(Resultado::class)->findBy(['torneo'=>1,'etapa'=>$etapaC]);
                $inscriptos = [];
                foreach($resultados as $resultado){
                    $inscriptos[] = $resultado->getGanador();
                }
            }
    
        }
        

        //alteramos el orden
        shuffle($inscriptos);

        $offset = 0;

        do {
            $partidosCreados = false; // Reiniciar la variable en cada iteración del bucle interior
        
            $contador = $offset;
            while ($contador < min($offset + 100, count($inscriptos))) {
                
                $control = $this->em->getRepository(Partido::class)->createQueryBuilder('p')
                ->andWhere('p.torneo = :torneo')
                ->andWhere('(:jugador IN (p.jugador_1, p.jugador_2))')
                ->andWhere('p.etapa = :etapa')
                ->setParameter('torneo', $torneo)
                ->setParameter('jugador', $inscriptos[$contador]->getJugador1())
                ->setParameter('etapa', $etapa)
                ->getQuery()
                ->getResult();
        
                if (count($control) == 0) {
                    $partido = new Partido();
                    $partido->setJugador1($inscriptos[$contador]->getJugador1());
                    $partido->setJugador2($inscriptos[$contador+1]->getJugador1());
                    $partido->setTorneo($torneo);
                    $partido->setEtapa($etapa);
                    $this->em->persist($partido);
                    $partidosCreados = true; 
                }
        
                $contador += 2; 
            }
        
            // Realizar el flush solo si se han creado partidos nuevos
            if ($partidosCreados) {
                $this->em->flush();
            }
        
            $this->em->clear(); 
        
            $offset = $contador;
        
        } while ($offset < count($inscriptos));
        
        
    }

   
}


