<?php

namespace App\Service;

use App\Entity\Jugador;
use App\Entity\Torneo;
use App\Entity\Resultado;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;

class OrganizarPartidos
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function inscripcionesAleatoreas(int $torneo, int $etapa )
    {
        $torneo = $this->em->getRepository(Torneo::class)->find(2);

        $jugadores = $this->em->getRepository(Jugador::class)->findAll();

        shuffle($jugadores);
        $maxJugadoresAleatorios = $torneo->getCantidad();

        $indicesAleatorios = array_rand($jugadores, $maxJugadoresAleatorios);
        $jugadoresInscritos = [];
        foreach ($indicesAleatorios as $indice) {
            $jugadoresInscritos[] = $jugadores[$indice];
        }

        foreach ($jugadoresInscritos as $jugador) {
            $inscripcion = new InscripcionTorneo();
            $inscripcion->setTorneo($torneo);
            $inscripcion->setJugador1($jugador);
    
            $this->em->persist($inscripcion);
        }
    
        $this->em->flush();
        
        
    }

   
}


