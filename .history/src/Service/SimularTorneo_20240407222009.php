<?php

namespace App\Service;

use App\Entity\Jugador;
use App\Entity\Resultado;
use App\Entity\Partido;
use App\Entity\Torneo;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;

class SimularTorneo
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function Etapa(int $torneo, int $etapa)
    {
        $partidos = $this->em->getRepository(Partido::class)->findBy(['torneo'=>$torneo,'etapa'=>$etapa]);
        $torneo = $this->em->getRepository(Torneo::class)->find($torneo);
        
        foreach($partidos as $partido){
           $ganador = $this->Partido($partido->getJugador1()->getId(),$partido->getJugador2()->getId());
           
           $resultadoExistente = $this->em->getRepository(Resultado::class)->findOneBy(['partido' => $partido]);

           if ($resultadoExistente !== null) {
            continue;
           }

            $resultado = new Resultado();
            $resultado->setPartido($partido);
            $resultado->setGanador($ganador);
            $resultado->setTorneo($torneo);
            $resultado->setEtapa($etapa);

            $this->em->persist($resultado);          
        }

        $this->em->flush();
    }

    public function Partido(int $idJugador1, int $idJugador2)
    {
        try {
        $jugador_1 = $this->em->getRepository(Jugador::class)->find($idJugador1);
        $jugador_2 = $this->em->getRepository(Jugador::class)->find($idJugador2);

        } catch (\Exception $e) {
            echo 'Se produjo un error al recuperar el jugador: ' . $e->getMessage();
        }


        //comprobamos si es un partido de hombres
        if($jugador_1->isGenero() and $jugador_2->isGenero()){

            $set = 10;
            $setJugados = 0;
            $diferencia = 0;
            $contadorj1 = 0;
            $contadorj2 = 0;
            $iteraciones = 0;
            $maxIteraciones = 20;

            while (($setJugados < $set || ($setJugados == $set && $diferencia < 2)) && $iteraciones <= $maxIteraciones) {
                 
                $rendimiento_j1 = $this->rendimiento($jugador_1->getHabilidad());

                dd();
                $rendimiento_j2 = $this->rendimiento($jugador_2->getHabilidad());

                //simula el punto
                if($rendimiento_j1 > $rendimiento_j2){

                    $contadorj1++;
                }else{
                    $contadorj2++;
                }

                 // Verificar si un jugador ha ganado el set
                if ($contadorj1 >= 6 && $contadorj1 >= $contadorj2 + 2) {
                    // Jugador 1 gana el set
                    break; // Salir del bucle de simulación del set
                } elseif ($contadorj2 >= 6 && $contadorj2 >= $contadorj1 + 2) {
                    // Jugador 2 gana el set
                    break; // Salir del bucle de simulación del set
                }

                $setJugados++;
                $iteraciones++;

            }

            if($contadorj1 > $contadorj2){
                $ganador = $jugador_1;
            }else{
                $ganador = $jugador_2;
            }
            return $ganador;
        }
        
    }

    public function rendimiento(int $habilidad)
    {
        $nroHabilidades = 6;
        $suerte = rand(0, 400);
       $habilidadTotal = $habilidad + $suerte;

       $porcentaje =  $habilidadTotal * 100 / ($nroHabilidades * 100);

       return $porcentaje;

    }
}


