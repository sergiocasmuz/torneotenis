<?php

namespace App\Service;

use App\Entity\Jugador;
use App\Entity\Torneo;
use App\Entity\InscripcionTorneo;
use Doctrine\ORM\EntityManagerInterface;

class Inscripciones
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function inscripcionesAleatoreas(int $torneo )
    {
        $torneo = $this->em->getRepository(Torneo::class)->find($torneo);
        $
        $jugadores = $this->em->getRepository(Jugador::class)->findAll();

        shuffle($jugadores);
        $totalInscriptos = count($this->em->getRepository(InscripcionTorneo::class)->findBy([ 'torneo'=>$torneo]));
        $maxJugadoresAleatorios = $torneo->getCantidad() - $totalInscriptos;
        $maxJugadoresAleatorios = min($maxJugadoresAleatorios, count($jugadores));

        if ($maxJugadoresAleatorios > 0) {
            $indicesAleatorios = array_rand($jugadores, min($maxJugadoresAleatorios, count($jugadores)));
            $indicesAleatorios = is_array($indicesAleatorios) ? $indicesAleatorios : [$indicesAleatorios]; // Convertir a array si es un entero
            $jugadoresInscritos = [];
            foreach ($indicesAleatorios as $indice) {
                $jugadoresInscritos[] = $jugadores[$indice];
            }
        
            foreach ($jugadoresInscritos as $jugador) {
                $control = $this->em->getRepository(InscripcionTorneo::class)->findBy(['jugador1'=>$jugador, 'torneo'=>$torneo]);

                if(!$control){
                    $inscripcion = new InscripcionTorneo();
                    $inscripcion->setTorneo($torneo);
                    $inscripcion->setJugador1($jugador);
                    $this->em->persist($inscripcion);
                }
        
                
            }
        
            $this->em->flush();

        }
        
        
    }

   
}


