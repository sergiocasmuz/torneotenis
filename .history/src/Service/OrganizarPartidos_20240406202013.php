<?php

namespace App\Service;

use App\Entity\Jugador;
use App\Entity\Torneo;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;

class OrganizarPartidos
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function organiza(Torneo $torneo)
    {
        $torneo = $this->em->getRepository(Torneo::class)->find($torneo);
        $inscriptos = $this->em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>$torneo]);
        $etapa =1;

        //alteramos el orden
        shuffle($inscriptos);

        $offset = 0;

        do {
            $partidosCreados = false; // Reiniciar la variable en cada iteración del bucle interior
        
            $contador = $offset;
            while ($contador < min($offset + 100, count($inscriptos))) {
                $control = $this->em->getRepository(Partido::class)->findBy([
                    'torneo' => $torneo, 
                    'jugador_1' => $inscriptos[$contador]->getJugador1(),
                    'jugador_2' => $inscriptos[$contador+1]->getJugador1(),
                    'etapa' => $etapa
                ]);
        
                if (count($control) == 0) {
                    $partido = new Partido();
                    $partido->setJugador1($inscriptos[$contador]->getJugador1());
                    $partido->setJugador2($inscriptos[$contador+1]->getJugador1());
                    $partido->setTorneo($torneo);
                    $partido->setEtaPA($torneo);
                    $this->em->persist($partido);
                    $partidosCreados = true; 
                }
        
                $contador += 2; 
            }
        
            // Realizar el flush solo si se han creado partidos nuevos
            if ($partidosCreados) {
                $this->em->flush();
            }
        
            $this->em->clear(); 
        
            $offset = $contador;
        
        } while ($offset < count($inscriptos));
        
        
    }

   
}


