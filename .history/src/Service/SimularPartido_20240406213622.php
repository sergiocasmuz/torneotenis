<?php

namespace App\Service;

use App\Entity\Jugador;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;

class SimularPartido
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function Simular(int $idJugador1, int $idJugador2)
    {
        try {
        $jugador_1 = $this->em->getRepository(Jugador::class)->find($idJugador1);
        $jugador_2 = $this->em->getRepository(Jugador::class)->find($idJugador2);

        } catch (\Exception $e) {
            echo 'Se produjo un error al recuperar el jugador: ' . $e->getMessage();
        }

    


        //comprobamos si es un partido de hombres
        if($jugador_1->isGenero() and $jugador_2->isGenero()){

            $set = 10;
            $setJugados = 0;
            $diferencia = 0;
            $contadorj1 = 0;
            $contadorj2 = 0;
            $iteraciones = 0;
            $maxIteraciones = 20;
           
            
            
            dump($jugador_1->getNombre()." ----- habilidad:".$jugador_1->getHabilidad());
            dump($jugador_2->getNombre()." ----- habilidad:".$jugador_2->getHabilidad());


            while (($setJugados < $set || ($setJugados == $set && $diferencia < 2)) && $iteraciones <= $maxIteraciones) {
                 
                $rendimiento_j1 = $this->rendimiento($jugador_1->getHabilidad());
                $rendimiento_j2 = $this->rendimiento($jugador_2->getHabilidad());

                //simula el punto
                if($rendimiento_j1 > $rendimiento_j2){

                    $contadorj1++;

                    echo "gana el jugador 1 <br>";
                }else{
                    $contadorj2++;
                    echo "gana el jugador 2 <br>";
                }


                 // Verificar si un jugador ha ganado el set
                if ($contadorj1 >= 6 && $contadorj1 >= $contadorj2 + 2) {
                    // Jugador 1 gana el set
                    break; // Salir del bucle de simulación del set
                } elseif ($contadorj2 >= 6 && $contadorj2 >= $contadorj1 + 2) {
                    // Jugador 2 gana el set
                    break; // Salir del bucle de simulación del set
                }

                $setJugados++;
                $iteraciones++;

            }

            dump($contadorj1);
                dump($contadorj1);

            if($contadorj1 > $contadorj1){
                $ganador = $contadorj1;
            }elseif(){
                
            }
            dd("fin del partido");
        }
        
    }

    public function rendimiento(int $habilidad)
    {
        $nroHabilidades = 6;
        $suerte = rand(0, 400);
       $habilidadTotal = $habilidad + $suerte;

       $porcentaje =  $habilidadTotal * 100 / ($nroHabilidades * 100);

       return $porcentaje;

    }
}


