<?php

namespace App\Service;

use App\Entity\Jugador;
use App\Entity\Torneo;
use App\Entity\InscripcionTorneo;
use App\Entity\Partido;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityManager;

class SimularPartido
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this -> em = $em;
    }

    public function Simular(int $torneo)
    {
        $torneo = $this->em->getRepository(Torneo::class)->find($torneo);
        $inscriptos = $this->em->getRepository(InscripcionTorneo::class)->findBy(['torneo'=>$torneo]);

        //alteramos el orden
        shuffle($inscriptos);

        $control = $this->em->getRepository(Partido::class)->fin

        $contador=0;
        while ($contador < count($inscriptos)) {

            $partido = new Partido();
            $partido->setJugador1($inscriptos[$contador]->getJugador1());
            $partido->setJugador2($inscriptos[$contador+1]->getJugador1());
            $partido->setTorneo($torneo);
            $contador+=2;

            $em->persist($partido);
        }

        $em->flush();
        
        
    }

   
}


