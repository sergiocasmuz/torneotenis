<?php

namespace App\DataFixtures;

use App\Entity\Jugador;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use League\Csv\Reader;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Ruta del archivo CSV
        $csvFilePath = './src/DataFixtures/jugador.csv';

        // Crear un lector CSV
        $csv = Reader::createFromPath($csvFilePath);

        // Iterar sobre las filas del CSV y crear jugadores
        foreach ($csv as $row) {
            $jugador = new Jugador();
            $jugador->setNombre($row[0]); 
            $jugador->setApellido($row[1]);
            $jugador->setGenero($row[2]);
            $jugador->setSaque($row[3]);
            $jugador->setFondo($row[4]);
            $jugador->setVolea($row[5]);
            $jugador->setMovilidad($row[6]);
            $jugador->setResistencia($row[7]);
            $jugador->setVelocidad($row[8]);
            $jugador->setFuerza($row[9]);
            $jugador->setReaccion($row[10]);
            // Establecer otros atributos según corresponda

            $manager->persist($jugador);
        }

        $manager->flush();
    }
}
