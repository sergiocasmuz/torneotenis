<?php

namespace App\DataFixtures;

use App\Entity\Jugador;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use League\Csv\Reader;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Ruta del archivo CSV
        $csvFilePath = './src/DataFixtures/jugador.csv';

        // Crear un lector CSV
        $csv = Reader::createFromPath($csvFilePath);

        // Iterar sobre las filas del CSV y crear jugadores
        foreach ($csv as $row) {
            $jugador = new Jugador();
            $jugador->setNombre($row[0]); 
            $jugador->setApellido($row[1]);
            $jugador->setGenero($row[1]);
            $jugador->setSaque($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            $jugador->setApellido($row[1]);
            // Establecer otros atributos según corresponda

            $manager->persist($jugador);
        }

        $manager->flush();
    }
}
