<?php

namespace App\DataFixtures;

use App\Entity\Jugador;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use League\Csv\Reader;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // Ruta del archivo CSV
        $csvFilePath = '/jugador.csv';

        // Crear un lector CSV
        $csv = Reader::createFromPath($csvFilePath);

        // Iterar sobre las filas del CSV y crear jugadores
        foreach ($csv as $row) {
            $jugador = new Jugador();
            $jugador->setNombre($row[0]); // Suponiendo que la primera columna sea el nombre
            $jugador->setApellido($row[1]); // Suponiendo que la segunda columna sea el apellido
            // Establecer otros atributos según corresponda

            $manager->persist($jugador);
        }

        $manager->flush();
    }
}
