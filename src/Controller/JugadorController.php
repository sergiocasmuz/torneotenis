<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Jugador;
use App\Form\JugadorType;
use Symfony\Component\HttpFoundation\Request;

class JugadorController extends AbstractController
{
    #[Route('/jugador', name: 'app_jugador')]
    public function index(EntityManagerInterface $em,  Request $request): Response
    {
        $jugadores = $em->getRepository(Jugador::class)->findAll();
    
        $jugadorN = new Jugador();
        $form = $this->createForm(JugadorType::class, $jugadorN);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $jugadorN = $form->getData();
            
          
            $em->persist($jugadorN);
            $em->flush();

            return $this->redirectToRoute('app_jugador');
        }

        return $this->render('jugador/index.html.twig', [
            'controller_name' => 'JugadorController',
            'jugadores' => $jugadores,
            'form' => $form->createView()
        ]);
    }
}
