<?php

namespace App\Entity;

use App\Repository\ResultadoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ResultadoRepository::class)]
class Resultado
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne]
    private ?Partido $partido = null;

    #[ORM\ManyToOne]
    private ?Jugador $ganador = null;

    #[ORM\ManyToOne]
    private ?Torneo $torneo = null;

    #[ORM\Column(nullable: true)]
    private ?int $etapa = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartido(): ?Partido
    {
        return $this->partido;
    }

    public function setPartido(?Partido $partido): static
    {
        $this->partido = $partido;

        return $this;
    }

    public function getGanador(): ?Jugador
    {
        return $this->ganador;
    }

    public function setGanador(?Jugador $ganador): static
    {
        $this->ganador = $ganador;

        return $this;
    }

    public function getTorneo(): ?Torneo
    {
        return $this->torneo;
    }

    public function setTorneo(?Torneo $torneo): static
    {
        $this->torneo = $torneo;

        return $this;
    }

    public function getEtapa(): ?int
    {
        return $this->etapa;
    }

    public function setEtapa(?int $etapa): static
    {
        $this->etapa = $etapa;

        return $this;
    }
}
