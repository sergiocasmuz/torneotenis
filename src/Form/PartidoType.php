<?php

namespace App\Form;

use App\Entity\Jugador;
use App\Entity\Partido;
use App\Entity\Torneo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartidoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('torneo', EntityType::class, [
                'class' => Torneo::class,
                'choice_label' => 'id',
            ])
            ->add('jugador_1', EntityType::class, [
                'class' => Jugador::class,
                'choice_label' => 'id',
            ])
            ->add('jugador_2', EntityType::class, [
                'class' => Jugador::class,
                'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Partido::class,
        ]);
    }
}
