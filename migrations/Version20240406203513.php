<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240406203513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE jugador_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE torneo_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE jugador (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, apellido VARCHAR(255) NOT NULL, genero BOOLEAN NOT NULL, saque INT DEFAULT NULL, fondo INT NOT NULL, volea INT NOT NULL, movilidad INT NOT NULL, resistencia INT NOT NULL, velocidad INT DEFAULT NULL, fuerza INT DEFAULT NULL, reaccion INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE torneo (id INT NOT NULL, nombre VARCHAR(255) NOT NULL, genero BOOLEAN NOT NULL, cantidad INT NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE jugador_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE torneo_id_seq CASCADE');
        $this->addSql('DROP TABLE jugador');
        $this->addSql('DROP TABLE torneo');
    }
}
