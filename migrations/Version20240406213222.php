<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240406213222 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE inscripcion_torneo ADD torneo_id INT NOT NULL');
        $this->addSql('ALTER TABLE inscripcion_torneo ADD CONSTRAINT FK_F319C6CFA0139802 FOREIGN KEY (torneo_id) REFERENCES torneo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F319C6CFA0139802 ON inscripcion_torneo (torneo_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE inscripcion_torneo DROP CONSTRAINT FK_F319C6CFA0139802');
        $this->addSql('DROP INDEX IDX_F319C6CFA0139802');
        $this->addSql('ALTER TABLE inscripcion_torneo DROP torneo_id');
    }
}
